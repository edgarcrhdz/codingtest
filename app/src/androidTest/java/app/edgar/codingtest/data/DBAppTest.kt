package app.edgar.codingtest.data

import android.app.Application
import android.content.Context
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.edgar.codingtest.data.employe.EmployeDao
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.data.user.UserDao
import app.edgar.codingtest.data.user.UserModel
import com.google.common.truth.Truth.assertThat
import junit.framework.TestCase
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DBAppTest : TestCase(){

    lateinit var dbapp: DBApp
    lateinit var db: RoomDB
    lateinit var userDAO: UserDao
    lateinit var employeDao: EmployeDao

    @Before
    public override fun setUp() {
        super.setUp()

        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context,RoomDB::class.java).build()

        userDAO = db.userDao()
        employeDao = db.employeDao()
    }

    @After
    fun closeDB(){
        db.close()
    }

    @Test
    fun writeReadUser() = runBlocking{
        val userModel = UserModel(0,"Edgar")
        userDAO.addUser(userModel)
        var list = userDAO.testData()
        assertThat(list.contains(userModel)).isTrue()
        userDAO.deleteUser()
        list = userDAO.testData()
        assertThat(list.contains(userModel)).isFalse()

    }

    @Test
    fun writeReadEmployed() = runBlocking{
        val employeModel = EmployeModel(0,"Edgar", "Cruz", "0000","descripción", 5.5f)
        val employeModelList = listOf<EmployeModel>(employeModel)
        employeDao.addEmployes(employeModelList)
        var list = employeDao.testData()
        assertThat(list.contains(employeModel)).isTrue()
    }

}