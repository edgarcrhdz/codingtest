package app.edgar.codingtest.views.activitys.home

import android.view.View as V
import app.edgar.codingtest.data.employe.EmployeModel
import io.reactivex.Observable

interface HomeContract {

    interface Model{

        fun employeService(listener : HomeInteractor.onDataFetched)
        fun insertAllEmployes(list: List<EmployeModel>)
        fun getEmployesFromDatabase() : Observable<List<EmployeModel>>?
        fun deleteUser()
    }

    interface View{

        fun initLottie()
        fun initRecyclerView()
        fun initEmployeList(list: MutableList<EmployeModel>)
        fun getEmployesService()
        fun getEmployesDatabase()
        fun updateAdapter(updatedList: MutableList<EmployeModel>)
        fun ClicLogOut(view: V)
        fun navigateToActivity(activity: Class<*>)
    }

    interface Presenter{
        fun callEmployesService()
        fun callEmployesDatabase()
        fun logOut()
    }

}