package app.edgar.codingtest.views.activitys.login

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import app.edgar.codingtest.data.DBApp
import app.edgar.codingtest.data.user.UserModel
import androidx.lifecycle.lifecycleScope
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.views.activitys.home.HomeActivity
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class LogInInteractor: LogInContract.Model {

    var view: LogInActivity? = null

    interface onDataFetched {
        fun onSuccess(FetchedData : UserModel)
        fun onFailure()
    }

    override fun getUserFromDatabase(): Observable<List<UserModel>>? {
        return DBApp.database?.userDao()?.readAllData()!!
    }

    override fun getUser(listener: onDataFetched){
        DBApp.database?.userDao()?.readAllData()!!.take(1).subscribe {
            if(it.size > 0) {
                listener.onSuccess(it[0])
            }else{
                listener.onFailure()
            }
        }
    }

    override fun addUser(user: UserModel) {
        view!!.lifecycleScope.launch {
            DBApp.database?.userDao()?.addUser(user)
            this.cancel()
        }
    }

    override fun updateUser(user: UserModel) {
        view!!.lifecycleScope.launch {
            DBApp.database?.userDao()?.updateUser(user)
            this.cancel()
        }
    }


}
