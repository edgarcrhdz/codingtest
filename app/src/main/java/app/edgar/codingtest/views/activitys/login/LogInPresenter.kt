package app.edgar.codingtest.views.activitys.login

import androidx.lifecycle.lifecycleScope
import app.edgar.codingtest.base.BasePresenter
import app.edgar.codingtest.data.DBApp
import app.edgar.codingtest.data.user.UserModel
import app.edgar.codingtest.views.activitys.home.HomeActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

class LogInPresenter constructor(view: LogInActivity, interactor: LogInInteractor) : BasePresenter(), LogInContract.Presenter {

    var view: LogInContract.View? = null
    var interactor: LogInContract.Model? = null

    init {
        this.view = view
        this.interactor = interactor
        interactor.view = this.view as LogInActivity
    }

    override fun setUser(user: UserModel) {

        interactor!!.getUserFromDatabase()!!.take(1).subscribe {
            if (it.size == 0) {
                interactor!!.addUser(user)
            } else {
                interactor!!.updateUser(user)
            }
            view!!.navigateToActivity(HomeActivity::class.java, user.userName)
        }

    }

    override fun callUserActive(){
        interactor!!.getUser(object : LogInInteractor.onDataFetched{
            override fun onSuccess(FetchedData: UserModel) {
                view!!.navigateToActivity(HomeActivity::class.java, FetchedData.userName)
            }

            override fun onFailure() {
            }

        })
    }


}