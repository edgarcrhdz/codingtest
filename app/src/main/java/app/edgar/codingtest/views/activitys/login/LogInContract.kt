package app.edgar.codingtest.views.activitys.login
import app.edgar.codingtest.data.user.UserModel
import io.reactivex.Flowable
import io.reactivex.Observable
import android.view.View as V

interface LogInContract {

    interface Model{

        fun getUserFromDatabase(): Observable<List<UserModel>>?
        fun addUser(user: UserModel)
        fun updateUser(user: UserModel)
        fun getUser(listener: LogInInteractor.onDataFetched)

    }

    interface View{

        fun ClickLogIn(view: V)
        fun navigateToActivity(activity: Class<*>, name: String)
        fun getUser()

    }

    interface Presenter{

        fun setUser(user: UserModel)
        fun callUserActive()

    }

}