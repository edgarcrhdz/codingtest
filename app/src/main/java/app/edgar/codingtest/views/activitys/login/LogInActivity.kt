package app.edgar.codingtest.views.activitys.login

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import app.edgar.codingtest.R
import app.edgar.codingtest.base.BaseActivity
import app.edgar.codingtest.data.DBApp
import app.edgar.codingtest.data.user.UserModel
import app.edgar.codingtest.views.activitys.home.HomeActivity
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : BaseActivity<LogInPresenter>(), LogInContract.View {

    override fun createPresenter(context: Context): LogInPresenter {
        return LogInPresenter(this, LogInInteractor())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        getUser()
    }

    override fun ClickLogIn(view: View) {
        if(userNameEditText.text.toString() != "" && passwordEditText.text.toString() != "") {
            presenter.setUser(UserModel(0, userNameEditText.text.toString()))
        }else{
            Toast.makeText(this,R.string.loginerror,Toast.LENGTH_LONG).show()
        }
    }

    override fun navigateToActivity(activity: Class<*>, name: String) {
        var intent = Intent(this, activity)
        intent.putExtra("name", name)
        startActivity(intent)
        finish()
    }

    override fun getUser() {
        presenter.callUserActive()
    }


}