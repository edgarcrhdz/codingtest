package app.edgar.codingtest.views.activitys.details

import app.edgar.codingtest.base.BasePresenter
import app.edgar.codingtest.views.activitys.home.HomeContract

class DetailsPresenter constructor(view: DetailsActivity) : BasePresenter(), DetailsContract.Presenter{

    var view: DetailsContract.View? = null

    init {
        this.view = view
    }

}