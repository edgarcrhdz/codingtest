package app.edgar.codingtest.views.activitys.home

import app.edgar.codingtest.base.BaseActivity
import app.edgar.codingtest.base.BasePresenter
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.views.activitys.login.LogInActivity
import app.edgar.codingtest.views.activitys.login.LogInContract

class HomePresenter constructor(view: HomeActivity, interactor: HomeInteractor) : BasePresenter(), HomeContract.Presenter{

    var view: HomeContract.View? = null
    var interactor: HomeContract.Model? = null

    init {
        this.view = view
        this.interactor = interactor
        interactor.view = this.view as HomeActivity
    }


    override fun callEmployesService() {
        interactor!!.employeService(object : HomeInteractor.onDataFetched{
            override fun onSuccess(FetchedData: List<EmployeModel>) {
                interactor!!.insertAllEmployes(FetchedData)
                view!!.updateAdapter(FetchedData.toMutableList())
            }

            override fun onFailure() {
            }

        })
    }

    override fun callEmployesDatabase() {
        interactor!!.getEmployesFromDatabase()!!.take(1).subscribe {
            view!!.initEmployeList(it.toMutableList())
            view!!.initRecyclerView()
        }
    }

    override fun logOut() {
        interactor!!.deleteUser()
        view!!.navigateToActivity(LogInActivity::class.java)
    }


}