package app.edgar.codingtest.views.activitys.home

import androidx.lifecycle.lifecycleScope
import app.edgar.codingtest.data.DBApp
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.data.user.UserModel
import app.edgar.codingtest.retrofit.RetrofitClient
import app.edgar.codingtest.retrofit.RetrofitInterface
import app.edgar.codingtest.views.activitys.login.LogInActivity
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import retrofit2.Retrofit


class HomeInteractor : HomeContract.Model {

    var view: HomeActivity? = null

    interface onDataFetched {
        fun onSuccess(FetchedData : List<EmployeModel>)
        fun onFailure()
    }

    override fun employeService(listener : onDataFetched) {
        val retrofit = RetrofitClient.instance
        val jsonApi = retrofit.create(RetrofitInterface::class.java)
        val response = jsonApi.getEmployes()
        response.take(1).subscribeOn(Schedulers.io()).subscribe({
            listener.onSuccess(it.employees)
        },{error ->
            listener.onFailure()
        })


    }

    override fun insertAllEmployes(list: List<EmployeModel>) {
        view!!.lifecycleScope.launch {
            DBApp.database?.employeDao()!!.addEmployes(list)
            this.cancel()
        }
    }

    override fun getEmployesFromDatabase(): Observable<List<EmployeModel>>? {
        return DBApp.database?.employeDao()?.readAllData()!!
    }

    override fun deleteUser() {
        view!!.lifecycleScope.launch {
            DBApp.database?.userDao()!!.deleteUser()
            this.cancel()
        }
    }

}
