package app.edgar.codingtest.views.activitys.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.ColorFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import app.edgar.codingtest.R
import app.edgar.codingtest.adapters.EmployeAdapter
import app.edgar.codingtest.base.BaseActivity
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.views.activitys.login.LogInInteractor
import app.edgar.codingtest.views.activitys.login.LogInPresenter
import com.airbnb.lottie.LottieProperty
import com.airbnb.lottie.SimpleColorFilter
import com.airbnb.lottie.model.KeyPath
import com.airbnb.lottie.value.LottieValueCallback
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomePresenter>(), HomeContract.View {

    override fun createPresenter(context: Context): HomePresenter {
        return HomePresenter(this, HomeInteractor())
    }

    lateinit var adapter: EmployeAdapter
    var employeList: MutableList<EmployeModel> = mutableListOf(EmployeModel(-1,"No se encuentran datos aún, refresque la vista","","","",0f))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        val extras = intent.extras
        if(extras != null){
            var name = extras.getString("name", "Code Test")
            nameText.setText(name)
        }

        initLottie()
        getEmployesDatabase()
        getEmployesService()




    }

    override fun initLottie() {

        lottieHome.setImageAssetsFolder("assets");
        lottieHome.setAnimation("dash.json");
        lottieHome.speed = 0.4f

        val yourColor = ContextCompat.getColor(this, R.color.colorPrimary)
        val yourColor2 = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        val filter = SimpleColorFilter(yourColor)
        val filter2 = SimpleColorFilter(yourColor2)
        val keyPath = KeyPath("Capa 2 contornos", "**")
        val keyPath2 = KeyPath("Capa 1 contornos", "**")
        val callback = LottieValueCallback<ColorFilter>(filter)
        val callback2 = LottieValueCallback<ColorFilter>(filter2)
        lottieHome.addValueCallback(keyPath, LottieProperty.COLOR_FILTER, callback);
        lottieHome.addValueCallback(keyPath2, LottieProperty.COLOR_FILTER, callback2);


    }



    @SuppressLint("WrongConstant")
    override fun initRecyclerView(){
        employeRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        adapter = EmployeAdapter(this, employeList)
        employeRecyclerView.adapter = adapter
        adapter.updateData(employeList)

        swipeRefresh.setOnRefreshListener {
            getEmployesService()
            swipeRefresh.isRefreshing = false
        }
    }

    override fun initEmployeList(list: MutableList<EmployeModel>) {
        this.employeList = list
    }

    override fun getEmployesService() {
        presenter.callEmployesService()
    }

    override fun getEmployesDatabase() {
        presenter.callEmployesDatabase()
    }


    @SuppressLint("WrongConstant")
    override fun updateAdapter(updatedList: MutableList<EmployeModel>) {
        adapter.updateData(updatedList)
    }

    override fun ClicLogOut(view: View) {
        presenter.logOut()
    }

    override fun navigateToActivity(activity: Class<*>) {
        var intent = Intent(this, activity)
        startActivity(intent)
        finish()
    }


}