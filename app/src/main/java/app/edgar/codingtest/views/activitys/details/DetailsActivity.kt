package app.edgar.codingtest.views.activitys.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.edgar.codingtest.R
import app.edgar.codingtest.views.activitys.home.HomeContract
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_home.*

class DetailsActivity : AppCompatActivity(), DetailsContract.View {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val extras = intent.extras
        if(extras != null){
            var firstName = extras.getString("firstName", "")
            var lastName = extras.getString("lastName", "")
            var image = extras.getString("image", "")
            var description = extras.getString("description", "")
            var rating = extras.getFloat("rating")

            fullNameText.setText(firstName + " " + lastName)
            descriptionText.setText(description)
            ratingText.setText(rating.toString())
            Picasso.get().load(image).into(imageView)
        }

    }

}