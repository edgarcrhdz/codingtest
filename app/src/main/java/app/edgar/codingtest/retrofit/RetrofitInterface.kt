package app.edgar.codingtest.retrofit

import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.data.employe.mEmploye
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import java.util.*

interface RetrofitInterface {


    @GET("/")
    @Headers(
        "Content-Type:application/json",
        "lenguage:en"
    )
    fun getEmployes(): Observable<mEmploye>


}