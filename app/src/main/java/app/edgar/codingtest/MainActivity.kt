package app.edgar.codingtest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import app.edgar.codingtest.data.DBApp
import app.edgar.codingtest.views.activitys.home.HomeActivity
import app.edgar.codingtest.views.activitys.login.LogInActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DBApp.database?.userDao()?.readAllData()!!.take(1).subscribe {
            if (it.size > 0) {
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra("name", it[0].userName)
                startActivity(intent)
            } else {
                val intent = Intent(this, LogInActivity::class.java)
                startActivity(intent)
            }
        }
    }
}