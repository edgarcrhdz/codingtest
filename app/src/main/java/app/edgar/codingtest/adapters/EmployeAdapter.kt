package app.edgar.codingtest.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import app.edgar.codingtest.R
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.views.activitys.details.DetailsActivity
import kotlinx.android.synthetic.main.item_employe.view.*

class EmployeAdapter(val context: Context, var employeList: MutableList<EmployeModel>) : RecyclerView.Adapter<EmployeAdapter.EmployeRecyclerViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeRecyclerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_employe,parent,false)
        return EmployeRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return employeList.size
    }

    override fun onBindViewHolder(holder: EmployeRecyclerViewHolder, position: Int) {
        val employe = employeList[position]
        holder.bindItems(employe, position, context)
        //Picasso.get().load(article.imageUrl).into(holder.imageview)

    }


    inner class EmployeRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: EmployeModel, position: Int, context: Context) {
            itemView.nameText.setText(item.firstName + " " + item.lastName)
            if(item.id >= 0) {
                itemView.setOnClickListener {
                    val intent = Intent(context, DetailsActivity::class.java)
                    intent.putExtra("firstName", item.firstName)
                    intent.putExtra("lastName", item.lastName)
                    intent.putExtra("image", item.image)
                    intent.putExtra("description", item.description)
                    intent.putExtra("rating", item.rating)
                    context.startActivity(intent)
                }
            }
        }


    }

    fun updateData(updatedList : MutableList<EmployeModel>){
        employeList = updatedList
        this.notifyDataSetChanged()
    }
}