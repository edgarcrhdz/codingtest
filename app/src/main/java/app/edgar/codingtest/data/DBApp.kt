package app.edgar.codingtest.data

import android.app.Application
import android.content.Context
import androidx.room.Room

class DBApp(): Application(){

    companion object {
        var database: RoomDB? = null
    }

    override fun onCreate() {
        super.onCreate()
        database =  Room.databaseBuilder(applicationContext, RoomDB::class.java, "article_db")
            .fallbackToDestructiveMigration().build()
    }

}