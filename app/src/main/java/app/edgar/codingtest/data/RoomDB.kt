package app.edgar.codingtest.data

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteOpenHelper
import app.edgar.codingtest.data.employe.EmployeDao
import app.edgar.codingtest.data.employe.EmployeModel
import app.edgar.codingtest.data.user.UserDao
import app.edgar.codingtest.data.user.UserModel

@Database(entities = [EmployeModel::class, UserModel::class], version = 4, exportSchema = false)
abstract class RoomDB: RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun employeDao(): EmployeDao

}