package app.edgar.codingtest.data.employe

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employe_table")
data class EmployeModel (
    @PrimaryKey()
    val id: Int,
    val firstName: String,
    val lastName: String,
    val image: String,
    val description: String,
    val rating: Float
)