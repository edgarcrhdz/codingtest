package app.edgar.codingtest.data.user

import androidx.lifecycle.LiveData
import androidx.room.*
import app.edgar.codingtest.data.employe.EmployeModel
import io.reactivex.Flowable
import io.reactivex.Observable

@Dao
interface UserDao {

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        suspend fun addUser(user: UserModel)

        @Query("SELECT * FROM user_table ORDER BY id ASC")
        fun readAllData(): Observable<List<UserModel>>

        @Update()
        suspend fun updateUser(user: UserModel)

        @Query("DELETE FROM user_table")
        suspend fun deleteUser()

        @Query("SELECT * FROM user_table ORDER BY id ASC")
        fun testData(): List<UserModel>

}