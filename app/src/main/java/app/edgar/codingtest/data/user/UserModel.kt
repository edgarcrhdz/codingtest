package app.edgar.codingtest.data.user

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class UserModel (
    @PrimaryKey()
    val id: Int,
    val userName: String
)