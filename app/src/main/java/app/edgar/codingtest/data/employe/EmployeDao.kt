package app.edgar.codingtest.data.employe

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable
import java.util.*

@Dao
interface EmployeDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addEmployes(employes: List<EmployeModel>)

    @Query("SELECT * FROM employe_table ORDER BY id ASC")
    fun readAllData(): Observable<List<EmployeModel>>

    @Query("SELECT * FROM employe_table ORDER BY id ASC")
    fun testData(): List<EmployeModel>
    
}